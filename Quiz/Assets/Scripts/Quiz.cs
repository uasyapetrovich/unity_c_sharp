﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using System;

public class Quiz : MonoBehaviour
{
    [SerializeField] private GameObject[] screens = new GameObject[10]; // Quantity
    [SerializeField] private GameObject endScreen;

    private int questNum;
    private GameObject currentScreen;

    private void Start()
    {
        Questions();

        Shuffle(screens);

        SetState();
    }

    private static void Shuffle(GameObject[] arr)
    {
        //System.Random rand = new System.Random();

        for (int i = arr.Length - 1; i > 0; i--)
        {
            //int j = rand.Next(i+1);
            int rnd = UnityEngine.Random.Range(0, i + 1);

            GameObject a = arr[rnd];
            arr[rnd] = arr[i];
            arr[i] = a;
        }
    }

    public void SetState()
    {
        if (questNum >= screens.Length - 1)
        {
            if (questNum == screens.Length) // if endScreen has already shown the game starts from the begining
            {
                questNum = 0;
                Shuffle(screens);
                endScreen.SetActive(false);
                currentScreen = screens[questNum];
                currentScreen.SetActive(true);
                return;
            }
            currentScreen.SetActive(false);
            endScreen.SetActive(true);
            questNum = screens.Length;

        }
        else if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            currentScreen = screens[++questNum];
            currentScreen.SetActive(true);
        }
        else
        {
            endScreen.SetActive(false);
            currentScreen = screens[questNum];
            currentScreen.SetActive(true);
        }
    }

    private void Questions()
    {
        //screens[0].;
    }
}
